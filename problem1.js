const data = require('./data.js')


// ==== Problem #1 ====
// The dealer can't recall the information for a car with an id of 33 on his lot. Help the dealer find out which car has an id of 33 by calling a function that will return the data for that car. Then log the car's year, make, and model in the console log in the format of:
//"Car 33 is a *car year goes here* *car make goes here* *car model goes here*"

function problem1(data, id) {
    let found;
    for (indexOfObj = 1; indexOfObj < data.length; indexOfObj++) {
        if (data[indexOfObj].id == id) {
            found = `Car 33 is a ${data[indexOfObj].car_year} ${data[indexOfObj].car_make} ${data[indexOfObj].car_model}`
        }
    }
    if (found){
        return found
    }else{
        return ''
    }
}

const ret=problem1(data, 33)
console.log(ret)