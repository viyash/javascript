const data = require('./data.js')


// ==== Problem #6 ====
// A buyer is interested in seeing only BMW and Audi cars within the inventory.  Execute a function and return an array that only contains BMW and Audi cars.  Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.
function problem6(data) {
    let arr = [];
    for (keyIndex = 0; keyIndex < data.length; keyIndex++) {
        carName = data[keyIndex].car_make
        if(carName == 'BMW' || carName == 'Audi'){
            arr.push(data[keyIndex])  
        }
    }
    return JSON.stringify(arr)
}

const ret = problem6(data)
console.log(ret)

